# Changelog

## Unreleased

- Updated dependencies
- Added basic desktop notifications propagation
- Added external resources support
- Added step count reading

## v0.3.0 - 2022-09-01

- Replaced println with a proper logging
- Media players list now updates automatically and immediately
- Added file save dialog for firmware download
- Removed xdg-download filesystem permission for Flatpak
- Enabled GPU acceleration for Flatpak
- Fixed crash on startup if bluetooth adapter is disabled or missing


## v0.2.0 - 2022-08-18

- Replaced file chooser widget with the dialog window
- Removed unwanted toast notifications
- Implemented media player integration
- Various UI improvements


## v0.1.0 - 2022-08-10

Initial release
